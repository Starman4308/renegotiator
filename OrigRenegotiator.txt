using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using KSP;
using Contracts;
using Contracts.Parameters;
//using Toolbar;
using KSPPluginFramework;

namespace Renegotiator
{
	/**
	 * The Renegotiator class will handle contract adjustment, maybe GUI
	 * Note to self: add handling for TAC: Life Support.
	 * Add support for changing suborbital/orbital contracts to be outside atmosphere.
	 * Not robust to contracts with more than one destination.
	 * 
	 * Some code and much of the code structure adapted from Acerola's public-domain ZeroScienceContracts mod.
	 * Linky: http://forum.kerbalspaceprogram.com/threads/89297-0-24-2-ZeroScienceContracts-%28no-science-from-contracts%29
	 */
	[KSPAddon(KSPAddon.Startup.MainMenu, false)]
	public class Renegotiator : MonoBehaviour
	{
		public static Renegotiator rnInst;
		private PlanetaryInfo[] allPlanets;
		private PlanetaryInfo kerbinInfo;
		private Dictionary<string,PlanetaryInfo> planetMap; // Hashes planet name to PlanetaryInfo.
		private int nPlanets;
		public double globalMult = 1.0;
		private string[] origPlanetNames = {"SUN", "KERBIN", "MUN", "MINMUS", "MOHO", "EVE",
			"DUNA","IKE", "JOOL", "LAYTHE", "VALL", "BOP", "TYLO", "GILLY", "POL", "DRES", "EELOO"};
		// Forks should refrain from renaming Moho to "lonely space potato" or Eve to "Evil Planet 2".
		
		/*private Dictionary<string,double> origAtmos = new Dictionary<string, double>()
		{ {"KERBIN", 69078}, {"DUNA", 41447}, {"EVE", 96709}, {"LAYTHE", 52981}, {"JOOL", 138156} };
		Will probably be obsoleted by Arsonide's code to ensure orbits are actually in orbit in 0.90.*/

		
		bool doModifyContracts= false;
		static bool isInstantiated = false;
		static bool doRemoveEventsOnDestroy = true;
		Dictionary<string, Contract> adjustedContracts;
		static bool dictionaryLoaded = false;
		String currentGame;
		bool exploreReturn = false; // "Explore body" contracts assume 1-way.
		bool directAscentReturn = false; // Land-and-return assumes Apollo-like orbital rendezvous.
		bool adjustPenalties = true; // Apply multiplier to contract failure.
		
		//private IButton paramButton;
		
		void Start()
		{
			rnInst = this;
			nPlanets = FlightGlobals.Bodies.Count;
			loadVarsFromConfig();
			ConfigNode[] rnSettings = GameDatabase.Instance.GetConfigNodes("RENEGOTIATOR_SETTINGS");
			ConfigNode upperNode = rnSettings [0];
			bool.TryParse (upperNode.GetValue ("exploreReturn"), out exploreReturn);
			bool.TryParse (upperNode.GetValue ("directAscentReturn"), out directAscentReturn);
			bool.TryParse (upperNode.GetValue ("adjustPenalties"), out adjustPenalties);

			if (!isInstantiated)
			{
				//Debug.Log ("Start stuff is going.");
				GameEvents.Contract.onContractsListChanged.Add(new EventVoid.OnEvent(OnContractsListChange));

				//Debug.Log ("Start stuff is ending.");

				DontDestroyOnLoad(this);
				isInstantiated = true;
			}
			else
			{
				doRemoveEventsOnDestroy = false;

				Destroy(this);
			}
			//paramButton = ToolbarManager.Instance.add("Renegotiator", "rnButton");
			currentGame = "9ABAvtM6Bh6nEA3Dzxr2"; // Please let nobody use the same 20-char random string for his save's name.
		}

		void OnDestroy()
		{
			Debug.Log ("OnDestroy was reached.");
			if (doRemoveEventsOnDestroy)
			{
				GameEvents.Contract.onContractsListChanged.Remove(new EventVoid.OnEvent(OnContractsListChange));
			}
			doRemoveEventsOnDestroy = true;
		}

		/**
		 * As far as I can tell, this is how Acerola's structure works: the onContractsListChanged
		 * event gets triggered, setting doModifyContracts true. Then, the next time this mod's Update()
		 * method is called, it calls ModifyContracts(), which does stuff, and then resets the 
		 * doModifyContracts flag false.
		 */
		public void Update()
		{
			if (doModifyContracts)
			{
				ModifyContracts();
			}
		}

		/**
		 * Loads up the dictionary of contracts Renegotiator has seen before.
		 * 
		 * Code written by studying TriggerAu's MIT-licensed Kerbal Alarm Clock code. I think it's covered
		 * by my own code being MIT-licensed, but I thought I'd give explicit mention.
		 * https://github.com/TriggerAu/KerbalAlarmClock
		 */
		private void loadContractIDs() {
			Debug.Log ("Reached loading contract IDs. Num contracts: " + ContractSystem.Instance.Contracts.Count);
			adjustedContracts = new Dictionary<string, Contract> ();
			List<Contract> allContracts = ContractSystem.Instance.Contracts;
			if (KSP.IO.File.Exists<Renegotiator>(String.Format("AdjustedContracts-{0}.txt", HighLogic.CurrentGame.Title))) {
				KSP.IO.TextReader tr = KSP.IO.TextReader.CreateForType<Renegotiator>(String.Format("AdjustedContracts-{0}.txt", HighLogic.CurrentGame.Title));
				String line = tr.ReadLine ();
				while (line != null) {
					line = line.Trim ();
					if (line.Equals ("Contract GUIDs encountered")) {
						// Skip line
					} else {
						foreach (Contract contract in allContracts) {
							String contractID = contract.ContractGuid.ToString ();
							if (line.Equals(contractID)) {
								adjustedContracts.Add (contractID, contract);
								break;
							}
						}
					}
					line = tr.ReadLine ();
				}
				tr.Close ();
				dictionaryLoaded = true;
			}
			//AdjustPayouts (allContracts);
		}

		// Also KAC-inspired.
		private void saveContractIDs() {
			KSP.IO.TextWriter tw = KSP.IO.TextWriter.CreateForType<Renegotiator>(String.Format("AdjustedContracts-{0}.txt", HighLogic.CurrentGame.Title));
			tw.WriteLine ("Contract GUIDs encountered");
			foreach (string contractId in adjustedContracts.Keys) {
				tw.WriteLine ("" + contractId);
			}
			tw.Close ();
		}

		/**
		 * Holds information about CelestialBodies necessary for Renegotiator function.
		 */
		public class PlanetaryInfo
		{
			public readonly string name;
			public string origName;
			// origName is what the planet originally was (Duna for Mars, etc) and should be upper-case.
			// Example: name "Mars", origName "DUNA".
			readonly public CelestialBody body;
			// Various contract multipliers.
			public double landed;
			public double landAndReturn;
			public double atmosphere;
			public double atmoAndReturn;
			public double orbit;
			public double orbitAndReturn;
			public double flyby;

			// To the best of my knowledge, should be Kerbin-specific.
			public double otherLanded;
			public double otherAtmo;
			public double suborbit;
			public double geosync;
			public double geostat;
			public double molniya;
			public double tundra;
			
			// Stock planet dV requirements.
			public double ascend;
			public double transfer;
			public double escape;

			public PlanetaryInfo (string toName, CelestialBody theBody) {
				name = toName;
				body = theBody;

				// Default: every multiplier is 1.0. Should get set to 1.0 anyways, though.
				landed = landAndReturn = otherLanded = atmosphere = atmoAndReturn = otherAtmo = suborbit = 1.0;
				orbit = orbitAndReturn = flyby = geosync = geostat = molniya = tundra = 1.0;
			}

			/*public override bool Equals (Object obj) {
				PlanetaryInfo other = obj as PlanetaryInfo;
				if (other == null)
					return false;
				else
					return this.name.Equals (other.name);
			}*/
		}

        public class origPlanet
        {
            public readonly double gravP;
            public readonly string name;
            public readonly double sma;
            public readonly double ecc;
            public readonly double radius;
            public readonly double atmoHeight;
            public readonly double incl;
            public readonly bool hasAtmo;

            public origPlanet(ConfigNode node)
            {
                name = node.GetValue("name");
                gravP = double.Parse(node.GetValue("gravParameter"));   // GM.
                sma = double.Parse(node.GetValue("sma"));               // Semi-major axis
                ecc = double.Parse(node.GetValue("ecc"));               // Eccentricity
                radius = double.Parse(node.GetValue("radius"));         // Planet radius
                double atmoReadVal = 0;                                 // Avoids possible difficulties with readonly vars.
                hasAtmo = double.TryParse(node.GetValue("atmoHeight"), out atmoReadVal);
                atmoHeight = hasAtmo ? atmoReadVal : 0;                 // Atmosphere height (if applicable)
                incl = double.Parse(node.GetValue("inclination"));      // Orbital inclination
            }
        }

		/*public PlanetaryInfo getKerbinInfo() {
			return kerbinInfo;
		}

		public int getKerbinIndex() {
			return kerbinIndex;
		}

		public PlanetaryInfo getSunInfo() {
			return sunInfo;
		}

		public PlanetaryInfo getPlanetInfo(string name) {
			PlanetaryInfo ret;
			planetMap.TryGetValue(name, out ret);
			return ret;
		}

		public PlanetaryInfo getPlanetInfo(int index) {
			return allPlanets [index];
		}*/

		void OnContractsListChange()
		{
			doModifyContracts = true;
		}

		/**
		 * Do stuff here. This gets tripped soon after changes in contract list, and calls the meat.
		 */
		private void ModifyContracts()
		{
			if (HighLogic.CurrentGame != null)
			{
				if (HighLogic.CurrentGame.Mode == Game.Modes.CAREER)
				{
					if (ContractSystem.Instance != null)
					{
						if (ContractSystem.Instance.Contracts != null)
						{
							AdjustPayouts();
							doModifyContracts = false;
						}
					}
				}
				else
				{
					doModifyContracts = false;
				}
			}
		}

		/**
		 * Stores essential information about contracts: primarily a list of parameters, any non-Kerbin
		 * destination, and whether the contract is stupid and must die (jet engine on Minmus, etc).
		 * 
		 * Should generally construct by passing in Kerbin (as a default).
		 */
		public class ContractInfo {
			public List<ParamInfo> paramsInfo;
			public PlanetaryInfo destination;
			public bool doKill; // Kill if contract has invalid parameters (jet engine on Minmus, etc.).
			public ParamSituation situation;

			public ContractInfo(PlanetaryInfo home) {
				destination = home; // Will default to Kerbin.
				doKill = false;
			}
		}

		/**
		 * Stores essential information about a contract parameter: its ID (to trace it back later), its
		 * location, the location (surface, atmo, orbit, etc), and min/max altitudes.
		 */
		public class ParamInfo {
			private Renegotiator renegotiator;
			public string id;
			public string name;
			public PlanetaryInfo destination;
			public ParamSituation situation;
			public double minAlt; // If set to -1, not important.
			public double maxAlt;
			public bool doKill; // Kill if contract has invalid parameters (jet engine on Minmus, etc.).

			public ParamInfo(Renegotiator reneg) {
				renegotiator = reneg;
				doKill = false;
				minAlt = maxAlt = -1; // Default to "unimportant".
				destination = reneg.kerbinInfo;
			}

			public ParamInfo(Renegotiator reneg, ConfigNode node) : this(reneg) {
				if (node.HasValue("name")) 
					name = node.GetValue("name");
				if (node.HasValue("id"))
					id = node.GetValue("id");
				if (node.HasValue("body")) {
					int bodyNum = 1;
					int.TryParse(node.GetValue("body"), out bodyNum);
					destination = renegotiator.allPlanets[bodyNum];
				}
				if (node.HasValue("alt")) {
					double.TryParse(node.GetValue("alt"), out minAlt);
				}
			}
		}

		/**
		 * Assumes it's been passed a valid CONTRACT node.
		 * For now, is not robust to multiple non-Kerbin celestial bodies.
		 */
		private ContractInfo ParseContract (Renegotiator reneg, ConfigNode node)
		{
			ContractInfo ret = new ContractInfo (reneg.kerbinInfo);
			ConfigNode[] subnodes = node.GetNodes ("PARAM");
			int nSubnodes = subnodes.Length;
			if (node.HasValue ("body")) {
				int bodynum = 1;
				if (int.TryParse (node.GetValue ("body"), out bodynum)) {
					ret.destination = reneg.allPlanets [bodynum];
				}
			}
			string type = node.GetValue ("type");
			switch (type) {
			case "AltitudeRecord":
				ret.situation = ParamSituation.ATMOSPHERE;
				break;
			case "OrbitKerbin":
				ret.situation = ParamSituation.ORBIT;
				break;
			case "FirstLaunch":
				ret.situation = ParamSituation.LANDED;
				break;
			case "ReachSpace":
				ret.situation = ParamSituation.SUBORBIT;
				break;
			case "ExploreBody":
				ret.situation = exploreReturn ? ParamSituation.LAND_AND_RETURN : ParamSituation.LANDED;
				break;
			case "CollectScience":
				string location = node.GetValue ("location");
				switch (location) {
				case "Space":
					ret.situation = ParamSituation.ORBIT;
					break;
				case "Surface":
					ret.situation = ParamSituation.LANDED;
					break;
				}
				ret.situation = location.Equals ("Space") ? ParamSituation.ORBIT : ParamSituation.LANDED;
				break;
			case "RescueKerbal":
				ret.situation = ParamSituation.ORBIT_AND_RETURN;
				break;
			case "PartTest":
				string sit = node.GetValue ("sit");
				switch (sit) {
				case "LANDED":
					ret.situation = ParamSituation.LANDED;
					break;
				case "SPLASHED":
					ret.situation = ParamSituation.LANDED;
					break;
				case "FLYING": // Atmosphere
					ret.situation = ParamSituation.ATMOSPHERE;
					break;
				case "SUB_ORBITAL": // Suborbital (to fill in)
					ret.situation = ParamSituation.LANDED;
					break;
				case "ORBITING":
					ret.situation = ParamSituation.ORBIT;
					break;
				case "ESCAPING":
					ret.situation = ParamSituation.FLYBY;
					break;
				}
				break;
			case "PlantFlag":
				ret.situation = ParamSituation.LAND_AND_RETURN; // No Kerbal left behind.
				break;
			default:
				if (node.HasValue ("rnSit")) {
					sit = node.GetValue ("rnSit");
					switch (sit) {
					case "LANDED":
						ret.situation = ParamSituation.LANDED;
						break;
					case "LAND_AND_RETURN":
						ret.situation = ParamSituation.LAND_AND_RETURN;
						break;
					case "OTHER_LANDED":
						ret.situation = ParamSituation.OTHER_LANDED;
						break;
					case "ATMOSPHERE":
						ret.situation = ParamSituation.ATMOSPHERE;
						break;
					case "OTHER_ATMO":
						ret.situation = ParamSituation.OTHER_ATMO;
						break;
					case "ATMO_AND_RETURN":
						ret.situation = ParamSituation.ATMO_AND_RETURN;
						break;
					case "SUBORBIT":
						ret.situation = ParamSituation.SUBORBIT;
						break;
					case "ORBIT":
						ret.situation = ParamSituation.ORBIT;
						break;
					case "ORBIT_AND_RETURN":
						ret.situation = ParamSituation.ORBIT_AND_RETURN;
						break;
					case "GEOSYNC":
						ret.situation = ParamSituation.GEOSYNC;
						break;
					case "GEOSTAT":
						ret.situation = ParamSituation.GEOSTAT;
						break;
					case "MOLNIYA":
						ret.situation = ParamSituation.MOLNIYA;
						break;
					case "TUNDRA":
						ret.situation = ParamSituation.TUNDRA;
						break;
					case "FLYBY":
						ret.situation = ParamSituation.FLYBY;
						break;
					}
				} else {
					ret.situation = ParamSituation.UNKNOWN;
				}
				break;
			}
			return ret;
		}

		public enum ParamSituation
		{
			LANDED, OTHER_LANDED, LAND_AND_RETURN, ATMOSPHERE, OTHER_ATMO, ATMO_AND_RETURN, SUBORBIT, ORBIT,
			ORBIT_AND_RETURN, GEOSYNC, GEOSTAT, MOLNIYA, TUNDRA, FLYBY, UNKNOWN
		}

		/**
		 * Parses a parameter for information like body, situation, and min/max altitude. Will also in
		 * the future look for stupidity like "Test Jet Engine on Minmus". At this point, assumes rather
		 * coherent, one-place-at-once parameters.
		 */
		private ParamInfo ParseParameter (ConfigNode node, Renegotiator reneg)
		{
			ParamInfo ret = new ParamInfo (reneg);
			List<ConfigNode> paramNodes = new List<ConfigNode> (node.GetNodes ("PARAM"));
			paramNodes.Add (node); // Search over this and all subnodes.
			foreach (ConfigNode subnode in paramNodes) {
				if (node.HasValue ("body")) {
					int bodyNum = 1;
					int.TryParse (node.GetValue ("body"), out bodyNum);
					ret.destination = allPlanets [bodyNum];
				}
				
				// Look for an "rnsit" value, then a "sit" value, then a "location" value, then
				// look in the name for special-case handling.
				
				if (subnode.HasValue ("rnSit")) {
					string sit = node.GetValue ("rnSit");
					switch (sit) {
					case "LANDED":
						ret.situation = ParamSituation.LANDED;
						break;
					case "LAND_AND_RETURN":
						ret.situation = ParamSituation.LAND_AND_RETURN;
						break;
					case "OTHER_LANDED":
						ret.situation = ParamSituation.OTHER_LANDED;
						break;
					case "ATMOSPHERE":
						ret.situation = ParamSituation.ATMOSPHERE;
						break;
					case "OTHER_ATMO":
						ret.situation = ParamSituation.OTHER_ATMO;
						break;
					case "ATMO_AND_RETURN":
						ret.situation = ParamSituation.ATMO_AND_RETURN;
						break;
					case "SUBORBIT":
						ret.situation = ParamSituation.SUBORBIT;
						break;
					case "ORBIT":
						ret.situation = ParamSituation.ORBIT;
						break;
					case "ORBIT_AND_RETURN":
						ret.situation = ParamSituation.ORBIT_AND_RETURN;
						break;
					case "GEOSYNC":
						ret.situation = ParamSituation.GEOSYNC;
						break;
					case "GEOSTAT":
						ret.situation = ParamSituation.GEOSTAT;
						break;
					case "MOLNIYA":
						ret.situation = ParamSituation.MOLNIYA;
						break;
					case "TUNDRA":
						ret.situation = ParamSituation.TUNDRA;
						break;
					case "FLYBY":
						ret.situation = ParamSituation.FLYBY;
						break;
					}
				} else if (subnode.HasValue ("sit")) {
					string sit = subnode.GetValue ("sit");
					switch (sit) {
					case "LANDED":
						ret.situation = ParamSituation.LANDED;
						break;
					case "SPLASHED":
						ret.situation = ParamSituation.LANDED;
						break;
					case "FLYING": // Atmosphere
						ret.situation = ParamSituation.ATMOSPHERE;
						break;
					case "SUB_ORBITAL":
						ret.situation = ParamSituation.LANDED;
						break;
					case "ORBITING":
						ret.situation = ParamSituation.ORBIT;
						break;
					case "ESCAPING":
						ret.situation = ParamSituation.FLYBY;
						break;
					}
				} else if (subnode.HasValue ("location")) {
					string sit = subnode.GetValue ("location");
					ret.situation = sit.Equals ("Space") ? ParamSituation.ORBIT : ParamSituation.LANDED;
				} else if (subnode.HasValue ("name")) {
					// Here comes a bunch of stupidstupidstupid special-case handling.
					string sit = subnode.GetValue ("name");
					if (sit.Equals ("EnterOrbit")) {
						ret.situation = ParamSituation.ORBIT;
					} else if (sit.Equals ("LandOnBody")) {
						ret.situation = exploreReturn ? ParamSituation.LAND_AND_RETURN : ParamSituation.LANDED;
					} else if (sit.Equals ("LaunchVessel")) {
						ret.situation = ParamSituation.LANDED;
					} else if (sit.Equals ("AltitudeRecord")) {
						ret.situation = ParamSituation.ATMOSPHERE;
					} else if (sit.Equals ("ReachSpace")) {
						ret.situation = ParamSituation.SUBORBIT;
					}
				}
				double someAlt = -1;
				if (node.HasValue ("maxAlt")) {
					double.TryParse(node.GetValue("maxAlt"), out someAlt);
					ret.maxAlt = someAlt;
				}
				if (node.HasValue ("minAlt")) {
					double.TryParse(node.GetValue("minAlt"), out someAlt);
					ret.minAlt = someAlt;
				}
				if (node.HasValue ("alt")) {
					double.TryParse(node.GetValue("malt"), out someAlt);
					ret.minAlt = someAlt;
				}
			}
			return ret;
		}

		/**
		 * Does the contract funds adjustment.
		 */
		private void AdjustPayouts ()
		{
			try {
				String gameName = HighLogic.CurrentGame.Title;
				if (!currentGame.Equals (gameName) || !dictionaryLoaded) {
					loadContractIDs ();
				}
			} catch (Exception ex) {
				Debug.Log ("Exception on contract list change: " + ex.ToString ());
			}
			foreach (Contract contract in ContractSystem.Instance.Contracts) {
				if (contract.IsFinished ())
					continue;
				string contractGuid = contract.ContractGuid.ToString ();
				
				if (!adjustedContracts.ContainsKey (contractGuid)) {
					// God, why is this the best solution?
					// God, I thought you were merciful.
					ConfigNode contractNode = new ConfigNode ();
					contract.Save (contractNode);
					ContractInfo cInfo = ParseContract (this, contractNode);
					adjustContract(contract, cInfo);
					foreach (ContractParameter param in contract.AllParameters) {
						if (param.FundsCompletion < 1) {
							continue; // Don't bother adjusting 0-funds parameters.
						}
						param.Save(contractNode);
						ParamInfo pInfo = ParseParameter(contractNode, this);
						adjustParameter(param, pInfo);
					}
					adjustedContracts.Add (contractGuid, contract);
				}
			}
			saveContractIDs ();
		}
		
		private void adjustParameter (ContractParameter param, ParamInfo pInfo)
		{
			double mult = findAdjustment (pInfo.destination, pInfo.situation);
			param.FundsCompletion *= mult;
			if (adjustPenalties)
				param.FundsFailure *= mult;
		}
		
		private bool isInSpace (ParamSituation psit)
		{
			switch (psit) {
				case ParamSituation.SUBORBIT:
				case ParamSituation.ORBIT:
				case ParamSituation.ORBIT_AND_RETURN:
				case ParamSituation.GEOSYNC:
				case ParamSituation.GEOSTAT:
				case ParamSituation.MOLNIYA:
				case ParamSituation.TUNDRA:
					return true;
				default:
					return false;
			}
		}
		
		private void adjustContract (Contract contract, ContractInfo cInfo)
		{
			double mult = findAdjustment(cInfo.destination, cInfo.situation);
			contract.FundsAdvance *= mult;
			contract.FundsCompletion *= mult;
			if (adjustPenalties) 
				contract.FundsFailure *= mult;
		}
		
		private double findAdjustment (PlanetaryInfo pInfo, ParamSituation sit)
		{
			double mult = globalMult;
			switch (sit) {
				case ParamSituation.LANDED:
				return mult * pInfo.landed;
				case ParamSituation.LAND_AND_RETURN:
				return mult * pInfo.landAndReturn;
				case ParamSituation.OTHER_LANDED:
				return mult * pInfo.otherLanded;
				case ParamSituation.ATMOSPHERE:
				return mult * pInfo.atmosphere;
				case ParamSituation.ATMO_AND_RETURN:
				return mult * pInfo.atmoAndReturn;
				case ParamSituation.OTHER_ATMO:
				return mult * pInfo.otherAtmo;
				case ParamSituation.SUBORBIT:
				return mult * pInfo.suborbit;
				case ParamSituation.ORBIT:
				return mult * pInfo.orbit;
				case ParamSituation.ORBIT_AND_RETURN:
				return mult * pInfo.orbitAndReturn;
				case ParamSituation.GEOSYNC:
				return mult * pInfo.geosync;
				case ParamSituation.GEOSTAT:
				return mult * pInfo.geostat;
				case ParamSituation.MOLNIYA:
				return mult * pInfo.molniya;
				case ParamSituation.TUNDRA:
				return mult * pInfo.tundra;
				case ParamSituation.FLYBY:
				return mult * pInfo.flyby;
				case ParamSituation.UNKNOWN:
				default:
				return mult;
			}
		}

		private void loadVarsFromConfig()
		{
			ConfigNode[] rnSettings = GameDatabase.Instance.GetConfigNodes("RN_PLANET_VARS");
			ConfigNode upperNode = rnSettings [0];
			int numNodes = upperNode.nodes.Count;
			Debug.Log ("Number of nodes: " + numNodes);
			int numOrig = origPlanetNames.Length;

			allPlanets = new PlanetaryInfo[nPlanets];
			planetMap = new Dictionary<string, PlanetaryInfo> (nPlanets);
			for (int i = 0; i < nPlanets; i++) {
				CelestialBody bodyi = FlightGlobals.Bodies [i];
				allPlanets [i] = new PlanetaryInfo (bodyi.name, bodyi);
				planetMap.Add (bodyi.name, allPlanets [i]);
			}

			foreach (ConfigNode planet in upperNode.nodes) {
				string pName = planet.name;
				string origName = planet.GetValue ("origName").ToUpper();
				PlanetaryInfo pInfo = new PlanetaryInfo("cat",null); // Will get re-assigned.

				if (!planetMap.TryGetValue (pName, out pInfo)) {
					Debug.LogWarning ("Planet " + pName + " from config file does not appear to match any loaded planet.");
					continue;
				}

				pInfo.origName = origName;
				if (origName.Equals ("KERBIN")) {
					kerbinInfo = pInfo;
					/*for (int i = 0; i < allPlanets.Length; i++) {
						if (allPlanets [i].Equals (pInfo)) {
							kerbinIndex = i;
							break;
						}
					}*/
				}/* else if (origName.Equals ("SUN")) {
					sunInfo = pInfo;
				}*/

				if (!double.TryParse (planet.GetValue ("landed"), out pInfo.landed)) {
					pInfo.landed = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("otherLanded"), out pInfo.otherLanded)) {
					pInfo.otherLanded = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("landAndReturn"), out pInfo.landAndReturn)) {
					pInfo.landAndReturn = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("atmosphere"), out pInfo.atmosphere)) {
					pInfo.atmosphere = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("otherAtmosphere"), out pInfo.otherAtmo)) {
					pInfo.otherAtmo = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("atmoAndReturn"), out pInfo.atmoAndReturn)) {
					pInfo.otherAtmo = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("suborbit"), out pInfo.suborbit)) {
					pInfo.suborbit = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("orbit"), out pInfo.orbit)) {
					pInfo.orbit = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("orbitAndReturn"), out pInfo.orbitAndReturn)) {
					pInfo.orbitAndReturn = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("geosync"), out pInfo.geosync)) {
					pInfo.geosync = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("geostat"), out pInfo.geostat)) {
					pInfo.geostat = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("molniya"), out pInfo.molniya)) {
					pInfo.molniya = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("tundra"), out pInfo.tundra)) {
					pInfo.tundra = 1.0;
				}
				if (!double.TryParse (planet.GetValue ("flyby"), out pInfo.flyby)) {
					pInfo.flyby = 1.0;
				}
			}
		}
		
		/**
		 * Reset parameters.
		 */
		public void Parametrize() {
			ConfigNode[] rnSettings = GameDatabase.Instance.GetConfigNodes("RN_ORIG_DV");
			ConfigNode upperNode = rnSettings [0];
			int nNodes = upperNode.CountNodes;
		}
	}
	
	/**
	 * GUI elements adapted from TriggerAu's KSPPluginFramework:
	 * https://github.com/TriggerAu/KSPPluginFramework
	 * MIT-licensed.
	 * As usual with "adapted", I mean "stolen lock, stock, and barrel, but with permission".
	 */
	[KSPAddon(KSPAddon.Startup.MainMenu,false)]
	public class RNStartWindow : MonoBehaviourWindow {
		bool isUnix;
		RNWindow rnWindow;
	
		internal override void Awake()
        {
            WindowCaption = "Renegotiator";
            // top left width height
            WindowRect = new Rect(0, 0, 250, 50);
            Visible = true;
            // Used solely to ensure that l/r-shift is used instead of alt on Linux.
            // For some reason, alt doesn't work on Linux.
            isUnix = Environment.OSVersion.Platform == PlatformID.Unix;
        }
        
        internal override void Start() {
        	rnWindow = RNWindow.rnWindowInst;
        	this.TooltipsEnabled = true;
        	this.DragEnabled = true;
        }

        internal override void Update ()
		{
			//toggle whether its visible or not
			if (isUnix) {
				if ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) && Input.GetKeyDown(KeyCode.F11))
                	Visible = !Visible;
			} else {
				if ((Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) && Input.GetKeyDown(KeyCode.F11))
                	Visible = !Visible;
			}
        }

        internal override void DrawWindow (int id)
		{
			GUILayout.Label (new GUIContent ("Startup window", "Press alt-F11 (shift-F11 on Linux) to bring it back up."));
                        
			if (GUILayout.Button ("Set Renegotiator Multipliers. This does not actually work yet.")) {
				rnWindow.Visible = true;
				this.Visible = false;
			}
            
			if (GUILayout.Button ("Close this window")) {
				this.Visible = false;
			}
        }
	}
	
	[KSPAddon(KSPAddon.Startup.MainMenu,false)]
	public class RNWindow : MonoBehaviourWindow {
	
		public static RNWindow rnWindowInst;
		Renegotiator rn;
		
		internal override void Awake()
        {
            WindowCaption = "Renegotiator Parametrization";
            WindowRect = new Rect(0, 0, 500, 50);
            Visible = false;
            rnWindowInst = this;
        	this.TooltipsEnabled = true;
        	this.DragEnabled = true;
        	rn = null;
        }

        internal override void DrawWindow (int id)
		{
			GUILayout.Label (new GUIContent ("Set contract payout multipliers!"));
			
			if (GUILayout.Button ("Reparametrize")) {
				if (rn == null) {
					rn = Renegotiator.rnInst;
				}
			}
			
			if (GUILayout.Button ("Close")) {
				this.Visible = false;
			}
            /*GUILayout.Label(new GUIContent("Window Contents", "Here is a reallly long tooltip to demonstrate the war and peace model of writing too much text in a tooltip\r\n\r\nIt even includes a couple of carriage returns to make stuff fun"));
            GUILayout.Label(String.Format("Drag Enabled:{0}",DragEnabled.ToString()));
            GUILayout.Label(String.Format("ClampToScreen:{0}",ClampToScreen.ToString()));
            GUILayout.Label(String.Format("Tooltips:{0}",TooltipsEnabled.ToString()));

            if (GUILayout.Button("Toggle Drag"))
                DragEnabled = !DragEnabled;
            if (GUILayout.Button("Toggle Screen Clamping"))
                ClampToScreen = !ClampToScreen;

            if (GUILayout.Button(new GUIContent("Toggle Tooltips","Can you see my Tooltip?")))
                TooltipsEnabled = !TooltipsEnabled;
            GUILayout.BeginHorizontal();
            GUILayout.Label("Max Tooltip Width");
            TooltipMaxWidth=Convert.ToInt32(GUILayout.TextField(TooltipMaxWidth.ToString()));
            GUILayout.EndHorizontal();
            GUILayout.Label("Width of 0 means no limit");

            GUILayout.Label("Alt+F11 - shows/hides window");*/

        }
	}
}

